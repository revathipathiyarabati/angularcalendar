import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }
  public getInfo(): Observable<any> {
    const url = 'http://10.1.1.222:8080/api/events';
    return this.http.get<any>(url);
  }
}
