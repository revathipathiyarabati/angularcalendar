
import { addDays, addHours, endOfMonth, startOfDay, subDays } from 'date-fns';
import { ChangeDetectorRef, Component } from '@angular/core';
import { CalendarView, CalendarEvent, DateFormatterParams } from 'angular-calendar';
import { DataService } from './data.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  viewDate: Date = new Date();
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;

  setView(view: CalendarView) {
    this.view = view;
  }



  events: CalendarEvent[] = [

  ];

  activeDayIsOpen: boolean = true;

  list: String[] = [];
  index: number = 0;
  notifications: String = "";
  date: any[] = [];
  // month: any[] = [];
  // year: any[] = [];

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    this.list.splice(0);
    this.date.splice(0);
    console.log(events, "events from backend");
    if (events.length) {
      for (this.index = 0; this.index < events.length; this.index++) {

        console.log(events[this.index].title);
        console.log(events);

        this.list[this.index] = events[this.index].title;
        //   this.date[0] = events[this.index].start.getDate();
        //   this.month[0] = events[this.index].start.getMonth();
        //   this.year[0] = events[this.index].start.getFullYear();
        //  console.log(date,this.month[0],this.year[0],"Present DAte");

      }



    }
    else {
      // this.list.splice(0);

    }


    //let x=this.adminService.dateFormat(date)
    //this.openAppointmentList(x)
  }
  info = new Array<any>();

  //
  d!: number;
  month!: string;
  year!:string;

  constructor(public dataService: DataService, private modal: NgbModal,private cdr: ChangeDetectorRef) {
    this.cdr.markForCheck();

  }
  ngOnInit(): void {

    this.d = (new Date().getMonth() + 1);
    console.log("checking date number",typeof(this.d));
    if (this.d == 1) {
      this.month = "January";
    }
    else if(this.d == 2){
      this.month = "February";
    }
    else if(this.d == 3){
      this.month = "March";
    }
    else if(this.d == 4){
      this.month = "April";
    }
    else if(this.d == 5){
      this.month = "May";
    }
    else if(this.d == 6){
      this.month = "June";
    }
    else if(this.d == 7){
      this.month = "July";
    }
    else if(this.d == 8){
      this.month = "August";
    }
    else if(this.d == 9){
      this.month = "September";
    }
    else if(this.d == 10){
      this.month = "October";
    }
    else if(this.d == 11){
      this.month = "November";
    }
    else {
      this.month = "December";
    }
    this.year = new Date().getFullYear().toString();



    this.dataService.getInfo().subscribe(response => {

      // this.events = response;
      // console.log(this.info,"api info");
      console.log(response, "info");
      this.events = response.map((ele: any) => {
        return ({
          start: (startOfDay(ele.date)),

          title: ele.eventName,

        })
      })

      console.log("after", this.events);
    });
  }

  public monthViewColumnHeader({date, locale}: DateFormatterParams): string {
    return new Intl.DateTimeFormat(locale, {weekday: 'short'}).format(date).substr(0, 1);
  }




}