import { CalendarEvent } from 'angular-calendar';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  @Input() list: String[] = [];   


  constructor() { }

  ngOnInit(): void {
  }

}
